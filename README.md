# Time Series Analysis on Life Expectancy data of US and Australia in last Century

• Performed comparative time series analysis on life expectancy yearly data for two countries and forecasted expectancies.

• Interpreted the ACF and PACF of lag time series data to correctly determine the possibilities of MA or/and AR components.

• Validated the possible ARIMA models suggested by auto.arima (using the AIC/BIC criteria) with their linear regression fits.

• Inferred the trends of obtained forecasts on the basis of RMSE and MAE criteria and derived important conclusions.

***Techniques Used:*** ARIMA, auto.arima, AIC/BIC, Linear Regression, Ljung-Box Test, RMSE, MAE. (**R**)
